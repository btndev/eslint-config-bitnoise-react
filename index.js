module.exports = {
  extends: [
    'eslint-config-bitnoise',
    'plugin:react/recommended',
    'eslint-config-standard-react',
    'eslint-config-airbnb/rules/react',
    'eslint-config-airbnb/rules/react-a11y',
    'eslint-config-react-app',
    'eslint-config-bitnoise/rules',
  ],
  plugins: [
    'react',
    'jsx-a11y',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      generators: true,
      experimentalObjectRestSpread: true,
    },
  },
  rules: {
    'react/jsx-tag-spacing': [
      'error',
      {
        afterOpening: 'never',
        beforeSelfClosing: 'always',
      },
    ],
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: ['.js'],
      },
    ],
    'react/require-default-props': 0,
    'react/prefer-stateless-function': [
      'error',
      {
        ignorePureComponents: true,
      },
    ],
    'react/jsx-wrap-multilines': [
      'error',
      {
        declaration: true,
        assignment: true,
        return: true,
        arrow: true,
      },
    ],
    'react/no-typos': 'off',
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        'components': [
          'Link',
          'NavLink',
        ],
        "specialLink": [
          'to',
        ],
        "aspects": [
          "noHref",
          "invalidHref",
          "preferButton"
        ],
      },
    ],
  },
}
